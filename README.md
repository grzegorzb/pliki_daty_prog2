Zadanie do zrobienia w domu

Utworzyć nową klasę o nazwie FilesManipulations a następnie:
1.Napisać metodę wczytującą dowolny plik z dysku a następnie
wypisującą wszystkie linie numerując je
2.Napisać metodę zapisującą podany tekst do pliku
3.Napisać metodę dopisującą podany tekst do pliku na jego końcu
4.Napisać metodę wyświetlającą wszystkie pliki w danym katalogu o rozszerzeniu .txt
5.Napisać metodę wypisującą pierwszą linię z każdego pliku o danym rozszerzeniu w
danym katalogu
6.Napisać metodę wyszukującą podaną frazę we wszystkich plikach o zadanym
rozszerzeniu w zadanym katalogu


Utworzyć nową klasę DateTimeManipulations a następnie:
1.Napisać metodę pozwalającą na sprawdzenie czy w
danym miesiącu i roku jest piątek 13.
2.Napisać metodę wypisującą dzień tygodnia dla danego roku dla: Wigilii
Bożego Narodzenia, Nowego Roku, Święta Konstytucji 3 Maja.
3.Napisać metodę wypisującą nazwy dni tygodnia wszystkich pierwszych dni
miesiąca dla podanego roku
4.Napisać metodę wyświetlającą daty wszystkich niedziel dla danego miesiąca i
roku