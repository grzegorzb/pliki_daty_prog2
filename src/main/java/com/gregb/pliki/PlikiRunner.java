package com.gregb.pliki;

import com.gregb.EnumColours;

import java.io.IOException;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;

public class PlikiRunner {

    public static void main(String[] args) throws Exception {
        FilesManipulations fm = new FilesManipulations();
//  1.Napisać metodę wczytującą dowolny plik z dysku a następnie wypisującą wszystkie linie numerując je
        fm.zad1odczytZPliku("io_zadania_prog2.txt");
//  2.Napisać metodę zapisującą podany tekst do pliku
        fm.zad2ZapisTxtDoPliku("io_zad2ZapisdoPLikuNew.txt", StandardOpenOption.CREATE, LocalDateTime.now().format(DateTimeFormatter.ofPattern("u.MM.dd HH:mm:ss")) + " Zapisałem ten tekst do nowego pliku w metodzie statycznej zad2ZapisTxtDoPliku");
//  3.Napisać metodę dopisującą podany tekst do pliku na jego końcu
        fm.zad2ZapisTxtDoPliku("io_zad3ZapisdoPLikuAppend.txt", StandardOpenOption.APPEND, LocalDateTime.now().format(DateTimeFormatter.ofPattern("u.MM.dd HH:mm:ss")) + " Zapisałem ten tekst w " + LocalDateTime.now().getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.getDefault()) + " w metodzie statycznej zad2ZapisTxtDoPliku");
//  4.Napisać metodę wyświetlającą wszystkie pliki w danym katalogu o rozszerzeniu .txt
        System.out.println("********    Zadanie 4. Wypisz pliki zakonczone txt     ********");
        fm.zad4wypiszPliki("txt");
//  5.Napisać metodę wypisującą pierwszą linię z każdego pliku o danym rozszerzeniu w danym katalogu
        System.out.println("********    Zadanie 5. Wypisz pierwszą linie z plików zakonczonych txt     ********");
        fm.zad5wypiszPierwszaLinie("src\\main\\java\\com\\gregb\\pliki\\", "txt");
//  6.Napisać metodę wyszukującą podaną frazę we wszystkich plikach o zadanym rozszerzeniu w zadanym katalogu
        System.out.println("********"+EnumColours.ANSI_GREEN.code() +"    Zadanie 6. Wyszukaj podaną frazę we wszystkich plikach     "+EnumColours.ANSI_RESET.code() +"******** \n"+
            fm.zad6WyszukajFrazeWplikach("src\\main\\java\\com\\gregb\\pliki\\", "txt","druga linia pliku") );
    }


}
