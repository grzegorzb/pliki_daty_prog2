package com.gregb.pliki;

import com.gregb.EnumColours;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilesManipulations {
    private int si;

    public FilesManipulations() { }

    String zad6WyszukajFrazeWplikach(String dirPath, String fileExtension, String findThis) throws Exception {
        String[] wyn = {""};
        Path path = Paths.get(dirPath);
        Stream<Path> filWithExten = Files.list(path)
                .filter(e -> e.getFileName().toString().endsWith(fileExtension));

//        filWithExten      // Pierwsza wersja, wypisuje wszystkie pliki z zaznaczeniem gdzie jest szukana fraza
//                 .forEach(c-> {
//             try {
//                 wyn[0] += ("\n" + c.getFileName().toString()
//                         + " : " + (Files.lines(c.toAbsolutePath()).filter(p -> p.toUpperCase().contains(findThis.toUpperCase())).findAny().isEmpty() ? "brak" : "jest '"+findThis+"'"));
//             } catch (IOException e) {
//                 e.printStackTrace();
//             }
//         });
        filWithExten
                .forEach(c-> {
                    try(Stream<String> lin = Files.lines(c)) {
                        List<String> listLine=lin.collect(Collectors.toList());
                        for (String oneLine: listLine )
                            if (oneLine.toUpperCase().contains(findThis.toUpperCase()))
                                wyn[0] += ("\n" + c.getFileName().toString()+ " : "+ EnumColours.ANSI_RED.code() + oneLine);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        return "Fraza '" + findThis + "' znajduje sie w plikach:\n" + wyn[0];
    }

    protected void zad5wypiszPierwszaLinie(String dirPath, String fileExtension) throws IOException {
        Path path = Paths.get(dirPath);
        Files.list(path)
                .filter(e -> e.getFileName().toString().endsWith(fileExtension))
                .peek(e -> System.out.print(e.getFileName().toString()))
                .forEach(e -> {
                    try {
                        System.out.println(": " + Files.lines(e.toAbsolutePath()).findFirst().stream().collect(Collectors.joining()));
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                });
    }

    protected void zad4wypiszPliki(String fileExtension) throws IOException {
        Path path = Paths.get("src\\main\\java\\com\\gregb\\pliki\\");
        Files.list(path).filter(e -> e.getFileName().toString().endsWith(fileExtension)).forEach(System.out::println);
    }

    protected void zad2ZapisTxtDoPliku(String nazwaPliku, StandardOpenOption saveAttr, String txtDoZapisu) throws IOException {
//        2.Napisać metodę zapisującą podany tekst do pliku
        Path path = Paths.get("src\\main\\java\\com\\gregb\\pliki\\" + nazwaPliku);
        if (!Files.exists(path)) Files.createFile(path);
        Files.write(path, (txtDoZapisu + "\n").getBytes(), saveAttr);

    }

    protected void zad1odczytZPliku(String nazwaPliku) throws IOException {
//    1.Napisać metodę wczytującą dowolny plik z dysku a następnie wypisującą wszystkie linie numerując je
        Path path = Paths.get("src\\main\\java\\com\\gregb\\pliki\\" + nazwaPliku);
        System.out.println("********    Zadanie 1. wersja z for     ********");
        int i = 0;
        for (String x : Files.lines(path).collect(Collectors.toList())) {
            System.out.println((++i) + " " + x);
        }
        System.out.println("+++     Zadanie 1. jako Stream     +++");
        si = 0;
        Files.lines(path)
                .map(e -> e = (++si) + " " + e) //nie rozumiem dlaczego tu nie moglem uzyc zmiennej wewnetrznej metody
                .forEach(System.out::println);

    }

}
