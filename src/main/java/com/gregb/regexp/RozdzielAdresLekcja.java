package com.gregb.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RozdzielAdresLekcja {
    public static void main(String[] args) {
        String txt ="ul. Hetmańska 120 35-600 Rzeszów";
        String p = "[A-Z][a-z]*[ ][A-Z][a-z]*";

        p = "[ul]";
        Pattern pattern = Pattern.compile(p);
        Matcher matcher = pattern.matcher(txt);

        System.out.println(matcher.regionStart()+" "+matcher.regionEnd() +" "+matcher.end("ul"));
    }
}
