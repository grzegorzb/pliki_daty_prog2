package com.gregb.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImieNazwiskoLekcja {
    public static void main(String[] args) {
        String txt ="Jan Kowalski";
        String p = "[A-Z][a-z]*[ ][A-Z][a-z]*";
        Pattern pattern = Pattern.compile(p);
        Matcher matcher = pattern.matcher(txt);

//        System.out.println("Podany tekst "+txt+" spelnia warunek "+matcher.matches());
        System.out.println("Podany tekst "+txt+ (matcher.matches()?" spelnia warunek ":" nie spelnia warunku "));

    }
}
