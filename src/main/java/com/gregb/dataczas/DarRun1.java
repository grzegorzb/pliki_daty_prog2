package com.gregb.dataczas;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DarRun1 {

        public static void main(String[] args) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

            // create a calendar and set
            Calendar cal = Calendar.getInstance();
            cal.setFirstDayOfWeek(Calendar.SUNDAY);

            cal.set(2020, 5, 29, 18, 20, 30);

            cal.setLenient(true);
            System.out.println("Calendar is Lenient : " + cal.isLenient());
            System.out.printf("Time Zone Name      : %s\n", cal.getTimeZone().getDisplayName());
            System.out.printf("getDisplayNames      : %s\n", cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ENGLISH));

            // Print Calendar's field
            System.out.println("Year            : " + cal.get(Calendar.YEAR));
            System.out.println("Month           : " + cal.get(Calendar.MONTH));
            System.out.println("Is May?         : " + (cal.get(Calendar.MONTH) == Calendar.MAY));
            System.out.println("Day of Month    : " + cal.get(Calendar.DAY_OF_MONTH));
            System.out.println("Day of Week     : " + cal.get(Calendar.DAY_OF_WEEK));
            System.out.println("Is Sunday?      : " + (cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY));
            System.out.println("Hour of the Day : " + cal.get(Calendar.HOUR_OF_DAY));
            System.out.println("Minute          : " + cal.get(Calendar.MINUTE));
            System.out.println("Second          : " + cal.get(Calendar.SECOND));
            System.out.println("AM PM           : " + cal.get(Calendar.AM_PM));
            System.out.println("Is AM?          : " + (cal.get(Calendar.AM_PM) == Calendar.AM));
            System.out.println("Is PM?          : " + (cal.get(Calendar.AM_PM) == Calendar.PM));
            System.out.println();

            // Displaying date using getTime()
            System.out.println("Current Date     : " + sdf.format(cal.getTime()));

            // Manipulating dates
            Calendar clonedCal = (Calendar) cal.clone();
            clonedCal.add(Calendar.DAY_OF_YEAR, -100);
            System.out.println("100 days ago was : " + sdf.format(clonedCal.getTime()));

            cal.add(Calendar.MONTH, 6);
            System.out.println("6 months later is: " + sdf.format(cal.getTime()));
        }

}
