package com.gregb.dataczas;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Locale;

public class DateTimeManipulations {
    public static void main(String[] args) {
        DateTimeManipulations dm = new DateTimeManipulations();
        System.out.println("Czy w podanym roku i miesiącu jest piątek 13tego: " + dm.isFriday13BeforeJ8(2020, Calendar.MAY));
        System.out.println("Czy w marcu 2020 jest piątek 13tego: " + dm.isFriday13BeforeJ8(2020, Calendar.MARCH));
        System.out.println(dm.dayFestivalBeforeJ8(2020));
        System.out.println(dm.firstDayOfMonthAfterJ8(2020));
        System.out.println(dm.dateOfSundayAfterJ8(2020,8));
    }

    public char isFriday13BeforeJ8(int year, int month) {
//  1.Napisać metodę pozwalającą na sprawdzenie czy w danym miesiącu i roku jest piątek 13.
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, 13);
        return ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) ? 'Y' : 'N');
    }

    public String dayFestivalBeforeJ8(int year) {
//  2.Napisać metodę wypisującą dzień tygodnia dla danego roku dla: Wigilii Bożego Narodzenia, Nowego Roku, Święta Konstytucji 3 Maja.
        String ret;
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, Calendar.MAY, 3);
        ret="W roku "+year+" święto Konstytucji 3 maja będzie w "+calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.forLanguageTag("pl-PL"));
        calendar.set(year, Calendar.DECEMBER, 24);
        ret+=", wigilia będzie w "+calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.forLanguageTag("pl-PL"));
        calendar.set(year, Calendar.DECEMBER, 31);
        ret+=", sylwester będzie w "+calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.forLanguageTag("pl-PL"));
        return ret;
    }

    public String firstDayOfMonthAfterJ8(int year) {
//  3.Napisać metodę wypisującą nazwy dni tygodnia wszystkich pierwszych dni miesiąca dla podanego roku
        String ret="W roku "+year+" nazwy dni tygodnia wszystkich pierwszych dni miesiąca: ";
        LocalDate customDate;
        for (int m = 1; m <= 12; m++) {
            customDate = LocalDate.of(year, m, 1);
            ret+= ((m==1)?"":", ")+customDate.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("pl-PL"));
        }
        return ret+".";
    }

    public String dateOfSundayAfterJ8(int year, int month) {
//  4.Napisać metodę wyświetlającą daty wszystkich niedziel dla danego miesiąca i roku
        String ret="W roku "+year+" daty wszystkich niedziel dla miesiąca "+Month.of(month).getDisplayName(TextStyle.FULL, Locale.forLanguageTag("pl-PL"))+" : ";
        LocalDate customDate = LocalDate.of(year, (month), 1);
        customDate = customDate.plusDays(7-customDate.getDayOfWeek().getValue()); // pierwsza niedziela w miesiacu
        for (int m = 0; m < 5; m++) {
            ret+= ((m==0)?"":", ")+customDate.plusDays(m*7).format(DateTimeFormatter.ISO_LOCAL_DATE);
            if (customDate.plusDays(m*7+7).getMonthValue()!=month) m++;
        }
        return ret+".";
    }
}
