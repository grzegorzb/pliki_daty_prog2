package com.gregb.dataczas;

import java.util.Calendar;
import java.util.Date;

public class StackCal {
    private static Date getTimeForAnyDayInWeek(int nDay, int nHour, int nMin)
    {
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        Date date = Calendar.getInstance().getTime();
        c.setTime(date);
        c.set(Calendar.DAY_OF_WEEK, nDay);
        c.set(Calendar.HOUR_OF_DAY, nHour);
        c.set(Calendar.MINUTE, nMin);
        return c.getTime();
    }

    public static void main(String[] args)
    {
        Date start = getTimeForAnyDayInWeek(6, 19, 00);
        Date end = getTimeForAnyDayInWeek(8, 19, 00);
        Date c = new Date();

        if (start.before(c) && c.before(end))
            System.out.println("BLOCK");
        else
            System.out.println("SEND");
    }
}
