package com.gregb.dataczas;

import java.util.Calendar;
import java.util.Locale;

public class Runner {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(2020,5,29); //dla systemu 5 = czerwiec

        System.out.println(calendar.get(java.util.Calendar.DAY_OF_WEEK));
        System.out.println(calendar.get(Calendar.YEAR) +" / "+ calendar.get(Calendar.MONTH)+" / "+ calendar.get(Calendar.DATE));
        System.out.println(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG,Locale.ENGLISH) + " "+ calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG,Locale.ENGLISH)+"\n"+calendar.getTime()+"\n"+calendar.getTimeZone());

        if (calendar.get(java.util.Calendar.DAY_OF_WEEK) == java.util.Calendar.FRIDAY) {
            System.out.println("Friday");
        } else if (calendar.get(java.util.Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            System.out.println("SATURDAY");
        }
    }
}
