package com.gregb.dataczas;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class KevRun {
    public static void main(String[] args) {
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR,    2020);
        c.set(Calendar.DAY_OF_MONTH,    29);
        c.set(Calendar.MONTH,    Calendar.MAY);
        System.out.println(c.get(Calendar.DAY_OF_WEEK));
        System.out.println(f.format(c.getTime()));
        c.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
        System.out.println(c.get(Calendar.DAY_OF_WEEK));
        System.out.println(f.format(c.getTime()));
    }
}
